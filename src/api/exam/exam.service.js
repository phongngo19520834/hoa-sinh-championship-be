const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');

module.exports = {
    getAll: async function (page = 0, pageSize = 10) {
        const pageNum = parseInt(page, 10);
        const pageSizeNum = parseInt(pageSize, 10);
        const offset = pageNum * pageSizeNum;
        const limit = pageSizeNum;

        const questions = await models.exam.findAndCountAll({
            limit,
            offset,
            where: {}, // conditions
        });
        return questions;
    },

    searchByName: async function (name = '') {
        if (name === '') return [];

        const sql = `SELECT *
                FROM exams
                WHERE exams.name LIKE :name `;
        const exams = await models.sequelize.query(sql, {
            replacements: { name: `%${name}%` },
            type: models.sequelize.QueryTypes.SELECT,
        });

        return exams;
    },

    create: async function (data) {
        const exam = await models.exam.create(data);

        return exam;
    },

    getById: async function (id) {
        const found = await models.exam.findByPk(id);

        if (!found) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This exam does not exist.',
                true,
            );
        }

        return found;
    },

    updateById: async function (id, body) {
        const exam = await this.getById(id);
        exam.set(body);
        const updated = await exam.save();

        return updated;
    },

    deleteById: async function (id) {
        const affected = await models.exam.destroy({
            where: {
                id: id,
            },
        });

        if (affected === 0) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This exam does not exist.',
                true,
            );
        }

        return affected;
    },
};
