module.exports = (sequelize, Sequelize) => {
  class Exam extends Sequelize.Model {}

  Exam.init(
      {
        name: {
            type: Sequelize.STRING(50),
            allowNull: false,
        },
        maxScore: {
            type: Sequelize.INTEGER,
            defaultValue: 10,
            allowNull: false,
        },
        totalQuestionUserMustDo: {
          type: Sequelize.INTEGER,
          defaultValue: 40,
          allowNull: false
        },
        countQuestion: {
          type: Sequelize.INTEGER,
          defaultValue: 0
        }, 
        isComplete: {
          type: Sequelize.BOOLEAN,
          get() {
            return this.countQuestion >= this.totalQuestionUserMustDo;
          }
        }
      },
      {
          sequelize,
          modelName: 'exam',
          timestamps: false,
          underscored: true, //  Set the field option on all attributes to the snake_case version of its name
      },
  );

  /**
   * -------------- ASSOCIATION ----------------
   */
  
  Exam.associate = function (models) {
      Exam.hasMany(models.question, {
          foreignKey: 'question_id',
          as: 'question',
          onDelete: 'CASCADE',
      });
      Exam.hasMany(models.session);
  };
  
  Exam.incQuestion = async function (id) {
    const exam = await Exam.findOne({ where: { id } });
    exam.set({countQuestion: exam.countQuestion + 1});
    await exam.save();
    return true;
  };
  Exam.decQuestion = async function (id) {
    const exam = await Exam.findOne({ where: { id } });
    exam.set({countQuestion: exam.countQuestion - 1});
    await exam.save();
    return true;
  };

  return Exam;
};