const examService = require('./exam.service');
const questionService = require('../question/question.service');

module.exports = {
    getAll: async function (req, res, next) {
        try {
            const { page, pageSize } = req.query;
            const exams = await examService.getAll(page, pageSize);

            return res.json(exams);
        } catch (err) {
            return next(err);
        }
    },
    searchByName: async function (req, res, next) {
        try {
            const { name } = req.query;
            const exams = await examService.searchByName(name);
            return res.json(exams);
        } catch (err) {
            return next(err);
        }
    },

    create: async function (req, res, next) {
        try {
            const exam = await examService.create(req.body);

            return res.json(exam);
        } catch (err) {
            return next(err);
        }
    },

    getById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const exam = await examService.getById(id);

            return res.json(exam);
        } catch (err) {
            return next(err);
        }
    },

    questionList: async function (req, res, next) {
        try {
            const { id } = req.params;
            const { page, pageSize } = req.query;
            const exams = await questionService.getQuestionsByExamId(id, page, pageSize);

            return res.json(exams);
        } catch (err) {
            return next(err);
        }
    },

    searchQuestionInExamByContent: async function (req, res, next) {
        try {
            const { id } = req.params;
            const { name } = req.query;
            const exams = await questionService.searchQuestionInExamByContent(id, name);

            return res.json(exams);
        } catch (err) {
            return next(err);
        }
    },

    updateById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const cat = await examService.updateById(id, req.body);

            return res.json(cat);
        } catch (err) {
            return next(err);
        }
    },

    deleteById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const affected = await examService.deleteById(id);

            return res.json({ affected });
        } catch (err) {
            return next(err);
        }
    },
};
