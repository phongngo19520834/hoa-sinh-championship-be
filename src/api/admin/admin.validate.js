/* eslint-disable no-param-reassign */
const Joi = require('@hapi/joi');
const { schemaValidator } = require('../../common/schema-validator/utils');
const { rolesEnum } = require('./role.enum');

// const rolesSchema = Joi.object({
//     role: Joi.valid(...rolesEnum).required(),
// }).unknown(true);

// const idListSchema = Joi.object({
//     ids: Joi.array().required().items(Joi.number()),
// }).unknown(true);

const userUpdateSchema = Joi.object({
    username: Joi.number(),
}).unknown(true);

module.exports = {
    validateUpdate: function (req, res, next) {
        try {
            schemaValidator(userUpdateSchema, req.body);

            delete req.body.id;
            delete req.body.isActive;

            next();
        } catch (err) {
            next(err);
        }
    },

    // validateRoleUpdate: function (req, res, next) {
    //     try {
    //         schemaValidator(rolesSchema, req.body);

    //         next();
    //     } catch (err) {
    //         next(err);
    //     }
    // },

    // validateActivationUpdate: function (req, res, next) {
    //     try {
    //         schemaValidator(idListSchema, req.body);

    //         next();
    //     } catch (err) {
    //         next(err);
    //     }
    // },
};
