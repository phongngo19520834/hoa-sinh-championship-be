const adminService = require('./admin.service');

module.exports = {
    getAll: async (req, res, next) => {
        try {
            const user = await adminService.getAll();

            return res.json(user);
        } catch (error) {
            next(error);
        }
    },

    whoAmI: async (req, res, next) => {
        try {
            const { id } = req.user;
            const user = await adminService.whoAmI(id);

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    getById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const user = await adminService.getById(id);

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    updateById: async (req, res, next) => {
        try {
            const { id } = req.body;
            const user = await adminService.updateById(id, req.body);

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    deleteById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const affected = await adminService.deleteById(id);

            res.json({ affected });
        } catch (error) {
            next(error);
        }
    },

    create: async (req, res, next) => {
        try {
            const user = await adminService.create(req.body);
            res.json(user);
        } catch (error) {
            next(error);
        }
    },
};
