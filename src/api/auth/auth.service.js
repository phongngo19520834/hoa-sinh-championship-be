const models = require('../../models');
const adminServices = require('../admin/admin.service');
const { issueJWT } = require('../../common/crypto/utils');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const { passwordResetEmail } = require('../../common/email/send');

module.exports = {
    register: async function (body) {
        const user = await adminServices.createUser(body);

        const jwt = issueJWT({userId: user.id, roleId: user.roleId});

        return {
            user: { username: user.username },
            token: jwt.token,
            exprires: jwt.expires,
        };
    },

    login: async function (credentials, forRole) {
        if (forRole === 'user') {
            const info = await models.user.validateUserCredentials(
                credentials,
                forRole,
            );
            if (!info) {
                throw new AppError(
                    httpStatus.UNAUTHORIZED,
                    'Invalid User Code.',
                    true,
                );
            }

            const jwt = issueJWT({userId: info.id, roleId: info.roleId});

            return {
                user: {
                    code: info.code,
                    sessionId: info.sessionId,
                    fullName: info.fullName,
                    isActive: info.isActive,
                    sessionName: info.session.dataValues.name
                },
                token: jwt.token,
                exprires: jwt.expires,
            };
        }
        if (forRole === 'admin') {
            const info = await models.admin.validateUserCredentials(
                credentials,
                forRole,
            );
            if (!info) {
                throw new AppError(
                    httpStatus.UNAUTHORIZED,
                    'Invalid Credentials.',
                    true,
                );
            }

            const jwt = issueJWT({userId: info.id, roleId: info.roleId});

            return {
                user: { username: info.username },
                token: jwt.token,
                exprires: jwt.expires,
            };
        }
    },

    sendPasswordResetEmail: async function (body) {
        const { identifier } = body;

        const user = await adminServices.getUserByIdentifier(identifier);

        const psToken = await models.password_reset_token.upsert({
            userId: user.id,
            token: 'kkkkkkkkk',
            expires: new Date(Date.now() + 15 * 60 * 1000).toISOString(), // 15 minutes
        });

        await passwordResetEmail(user.email, psToken.token);

        return { message: 'Reset password email was sended.' };
    },
};
