const router = require('express').Router();
const auth = require('../auth/passport.strategy')();
const { checkPermission } = require('../auth/auth.permission');
const questionCtl = require('./question.controller');
const {
    validateQuestionCreate,
    validateQuestionUpdate,
} = require('./question.validate');

router.use(auth.authenticate);

router
    .route('/')
    .get(checkPermission(['admin']), questionCtl.getAll)
    .post(
        checkPermission(['admin']),
        validateQuestionCreate,
        questionCtl.create,
    );
router
    .route('/search')
    .get(checkPermission(['admin']), questionCtl.searchByContent);

router
    .route('/:id')
    .get(checkPermission(['admin']), questionCtl.getById)
    .patch(
        checkPermission(['admin']),
        validateQuestionUpdate,
        questionCtl.updateById,
    )
    .delete(checkPermission(['admin']), questionCtl.deleteById);

module.exports = router;
