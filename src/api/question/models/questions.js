/* CREATE TABLE "questions" (
    "id" int(11) NOT NULL AUTO_INCREMENT,
    "question_content" text NOT NULL,
    "answer_A" text NOT NULL,
    "answer_B" text NOT NULL,
    "answer_C" text NOT NULL,
    "answer_D" text NOT NULL,
    "image" varchar(255) DEFAULT NULL,
    "description" varchar(255) DEFAULT NULL,
    "createdAt" datetime NOT NULL,
    "updatedAt" datetime NOT NULL,
    PRIMARY KEY ("id")
) */

module.exports = (sequelize, Sequelize) => {
    class Questions extends Sequelize.Model {}

    Questions.init(
        {
            examId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            content: {
                type: Sequelize.TEXT,
                allowNull: false,
            },
            answerA: { type: Sequelize.TEXT, allowNull: true },
            answerB: { type: Sequelize.TEXT, allowNull: true },
            answerC: { type: Sequelize.TEXT, allowNull: true },
            answerD: { type: Sequelize.TEXT, allowNull: true },
            answerE: { type: Sequelize.TEXT, allowNull: true },
            result: {
                type: Sequelize.ENUM({
                    values: ['A', 'B', 'C', 'D', 'E'],
                }),
                allowNull: false,
            },
            image: { type: Sequelize.STRING(255) },
            description: { type: Sequelize.STRING(255) },
        },
        {
            sequelize,
            modelName: 'question',
            timestamps: false,
            underscored: true, //  Set the field option on all attributes to the snake_case version of its name
        },
    );

    Questions.associate = function (models) {
        Questions.belongsTo(models.exam, {
            foreignKey: 'examId',
            as: 'exam',
            onDelete: 'CASCADE',
        });
    };
    
    return Questions;
};
