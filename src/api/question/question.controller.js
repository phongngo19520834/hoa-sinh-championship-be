const questionService = require('./question.service');

module.exports = {

    getAll: async function (req, res, next) {
        try {
            const { page, pageSize } = req.query;
            const questions = await questionService.getAll(page, pageSize);

            return res.json(questions);
        } catch (err) {
            return next(err);
        }
    },
    searchByContent: async function (req, res, next) {
        try {
            const { content } = req.query;
            const questions = await questionService.searchByContent(content);
            return res.json(questions);
        } catch (err) {
            return next(err);
        }
    },

    create: async function (req, res, next) {
        try {
            const question = await questionService.create(req.body);

            return res.json(question);
        } catch (err) {
            return next(err);
        }
    },

    getById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const question = await questionService.getById(id);

            return res.json(question);
        } catch (err) {
            return next(err);
        }
    },

    updateById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const cat = await questionService.updateById(id, req.body);

            return res.json(cat);
        } catch (err) {
            return next(err);
        }
    },

    deleteById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const affected = await questionService.deleteById(id);

            return res.json({ affected });
        } catch (err) {
            return next(err);
        }
    },

};
