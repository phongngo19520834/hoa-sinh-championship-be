const userService = require('./user.service');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const sessionService = require('../session/session.service');

const { typesMap } = require('./type.enum');
const { universitiesMap } = require('./university.enum');
const { rolesMap } = require('../admin/role.enum');

module.exports = {
    // FOR USER ONLY

    whoAmI: async (req, res, next) => {
        try {
            const { id } = req.user;
            const user = await userService.whoAmI(id);

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    updateUserFirstLoginById: async (req, res, next) => {
        try {
            const { id } = req.user;
            const user = await userService.updateUserFirstLoginById(
                id,
                req.body,
            );

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    getRank: async (req, res, next) => {
        try {
            let { typeId, universityId } = req.query;
            const { page, pageSize } = req.query;

            if (!typeId) { typeId = typesMap.team }
            if (!universityId) { universityId = universitiesMap.none }
            const user = await userService.getRank(typeId, universityId, page, pageSize);

            return res.json(user);
        } catch (error) {
            next(error);
        }
    },

    // ----- FOR ADMIN ONLY ------------------

    create: async (req, res, next) => {
        try {
            if ( !req.body.sessionId) { 
                throw new AppError(
                    httpStatus.NOT_FOUND,
                    'No session Id in your data',
                    true,
                )
            }
            if ( !req.body.universityId) {
                const session = await sessionService.getById(req.body.sessionId);
                const { typeId } = session.dataValues;
                if (typeId === typesMap.single) { req.body.universityId = universitiesMap.none}
                if (typeId === typesMap.team) { req.body.universityId = universitiesMap.YD}
            }
            
            const user = await userService.createUser(req.body);

            return res.json(user);
        } catch (error) {
            next(error);
        }
    },

    createList: async (req, res, next) => {
        try {
            let { universityId }  = req.body;
            const { sessionId, listUser } = req.body;
            if ( !sessionId) { 
                throw new AppError(
                    httpStatus.NOT_FOUND,
                    'No session Id in your data',
                    true,
                )
            }
            if ( !universityId) {
                const session = await sessionService.getById( sessionId );
                const { typeId } = session.dataValues;
                if (typeId === typesMap.single) { universityId = universitiesMap.none}
                if (typeId === typesMap.team) { universityId = universitiesMap.YD}
            }

            const userArr = []
            JSON.parse(listUser).forEach(element => {
                userArr.push({...element, roleId: rolesMap.user, universityId, sessionId })
            });
            
            const result = await userService.createListUser(userArr);

            return res.json(result);
        } catch (error) {
            next(error);
        }
    },

    getAllUser: async (req, res, next) => {
        try {
            const { sessionId, page, pageSize } = req.query;
            const user = await userService.getAllUser(sessionId, page, pageSize);

            return res.json(user);
        } catch (error) {
            next(error);
        }
    },

    getUserById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const user = await userService.getUserById(id);

            delete user.dataValues.role;
            delete user.dataValues.roleId;
            delete user.dataValues.isActive;
            delete user.dataValues.id;

            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    searchUserByUserCode: async (req, res, next) => {
        try {
            const { userCode, page, pageSize } = req.query;
            const users = await userService.searchUserByUserCode(userCode, page, pageSize);

            res.json(users);
        } catch (error) {
            next(error);
        }
    },

    deleteUserById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const affected = await userService.deleteUserById(id);

            res.json({ affected });
        } catch (error) {
            next(error);
        }
    },

    updateUserById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const user = await userService.updateUserById(id, req.body);

            res.json(user);
        } catch (error) {
            next(error);
        }
    },
    resetUserResultByCode: async (req, res, next) => {
        try {
            const user = await userService.resetResultByCode(req.body.code);
            if (!user) {
                throw new AppError(
                    httpStatus.NOT_FOUND,
                    `User with code "${req.body.code}" was not exist.`,
                    true,
                );
            }
            // call resetUserResultByCode in service
            res.json(user);
        } catch (error) {
            next(error);
        }
    },

    // ------------------ NOT USE ---------------------

    activateUserByIds: async (req, res, next) => {
        try {
            const { ids } = req.body;
            const activated = await userService.activateUserByIds(ids);

            res.json({ activated });
        } catch (error) {
            next(error);
        }
    },

    updateUserRoleById: async (req, res, next) => {
        try {
            const { id } = req.params;
            const { role } = req.body;
            const granted = await userService.updateUserRoleById(id, role);

            res.json({ granted });
        } catch (error) {
            next(error);
        }
    },

};
