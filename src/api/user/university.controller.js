const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const wrapCatchAsync = require('../../common/error/catchAsync'); 

module.exports = {
  
    getAll: wrapCatchAsync(async function (req, res, next) {
      const universities = await models.university.findAndCountAll();
      return res.json(universities);
    }),

    create: wrapCatchAsync(async function (req, res, next) {
        const university = await models.university.create(req.body);

        return res.json(university);
    }),

    getById:  wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const found = await models.university.findByPk(id);

      if (!found) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This university does not exist.',
              true,
          );
      }
      return res.json(found);
    }),

    updateById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const university = await models.university.findByPk(id);
      university.set(req.body);
      const updated = await university.save();

      return res.json(updated);
    }),

    deleteById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;  
      const affected = await models.university.destroy({
            where: {
                id: id,
            },
        });

      if (affected === 0) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This university does not exist.',
              true,
          );
      }

      return res.json(affected);
    }),
};
