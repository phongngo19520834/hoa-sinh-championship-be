/* eslint-disable camelcase */
const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const { rolesMap } = require('./role.enum');

module.exports = {
    whoAmI: async function (id) {
        const user = await this.getUserById(id);

        delete user.dataValues.password;
        delete user.dataValues.historyQues;
        delete user.dataValues.historyAnss;
        delete user.dataValues.roleId;
        delete user.dataValues.role;
        delete user.dataValues.id;

        return user;
    },
    /* ================== FOR TEST ================== */
    isUserNotHaveInfo: async function (id) {
        const user = await this.getUserById(id);
        return user.fullName === '' ? false : user;
    },

    isUserCanStartExam: async function (id) {
        const user = await this.getUserById(id);
        return user.historyQues !== '' ? false : user;
    },
    isUserCanSubmitExam: async function (id) {
        const user = await this.getUserById(id);
        return user.historyAnss !== '' ? false : user;
    },

    getHistoryQuestions: async function (id) {
        const user = await this.getUserById(id);
        return user.historyQues;
    },

    startExam: async function (id, timeServerStart, quesStr) {
        const user = await models.user.update(
            {
                startTime: timeServerStart,
                historyQues: quesStr
            },
            {
                where: {
                    id: id,
                },
            },
        );
        return user;
    },
    finalExam: async function (id, time, arrayAns, score) {
        const user = await models.user.update(
            {
                time,
                historyAnss: arrayAns,
                score
            },
            {
                where: {
                    id: id,
                },
            },
        );
        return user;
    },
    updateHistoryQuestions: async function (id, questions) {
        const user = await models.user.update(
            {
                historyQues: questions,
            },
            {
                where: {
                    id: id,
                },
            },
        );
        return user;
    },
    updateHistoryAnss: async function (id, anss) {
        const user = await models.user.update(
            {
                historyAnss: anss,
            },
            {
                where: {
                    id: id,
                },
            },
        );
        return user;
    },

    // All function below is for USER only
    createUser: async function (dto) {
        const user = await models.user.create({
            ...dto,
            roleId: rolesMap.user, // user role
        });

        return user;
    },

    createListUser: async function(arr) {
        const result = await models.user.bulkCreate(arr);
        return result
    },

    updateUserFirstLoginById: async function (id, body) {
        const user = await this.getUserById(id);
        if (user.fullName !== '' || user.stdId !== '') {
            throw new AppError(
                httpStatus.BAD_REQUEST,
                'This user has been updated.',
                true,
            );
        }
        user.set(body);
        const updated = await user.save();

        delete updated.dataValues.roleId;
        delete updated.dataValues.role;
        delete updated.dataValues.id;

        return updated;
    },

    // All function below is for ADMIN only

    updateUserById: async function (id, body) {
        const user = await this.getUserById(id);
        user.set(body);
        const updated = await user.save();

        delete updated.dataValues.roleId;
        delete updated.dataValues.role;
        delete updated.dataValues.id;

        return updated;
    },

    getUserById: async function (id) {
        const found = await models.user.scope(['role', 'session']).findByPk(id);

        if (!found) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This user does not exist.',
                true,
            );
        }

        return found;
    },

    searchUserByUserCode: async function (userCode, page = 0, pageSize = 10) {
        const pageNum = parseInt(page, 10);
        const pageSizeNum = parseInt(pageSize, 10);
        const offset = pageNum * pageSizeNum;
        const limit = pageSizeNum;

        const info = await models.user.findAndCountAll({
            limit,
            offset,
            where: {
                code: {
                    [models.Sequelize.Op.like]: `%${userCode}%`
                }
            },
        });
        return info;
    },
    
    resetResultByCode: async function (code) {
        const user = await models.user.findOne({
            where: {
                userCode: code,
            },
        });
        if (!user) return null;
        user.historyQues = '';
        user.historyAnss = '';
        user.startTime = null;
        user.time = null;
        user.score = null;
        const updated = await user.save();
        return updated;
    },

    deleteUserById: async function (id) {
        const deleted = await models.user.destroy({
            where: {
                id: id,
                roleId: [rolesMap.user, rolesMap.editor],
            },
        });

        if (deleted === 0) {
            throw new AppError(
                httpStatus.NOT_FOUND,
                'This user does not exist.',
                true,
            );
        }

        return deleted;
    },

    getAllUser: async function (sessionId, page = 0, pageSize = 10) {
        const pageNum = parseInt(page, 10);
        const pageSizeNum = parseInt(pageSize, 10);
        const offset = pageNum * pageSizeNum;
        const limit = pageSizeNum;

        let option = {}
        if (!sessionId) {
            option = {
                limit,
                offset,
                where: {
                    roleId: rolesMap.user,
                },
                order: [
                    ['id', 'DESC'],
                ]
            }
        } else {
            option = {
                limit,
                offset,
                where: {
                    sessionId,
                    roleId: rolesMap.user,
                },
                order: [
                    ['id', 'DESC'],
                ]
            }
        }
        const info = await models.user.findAndCountAll(option);
        return info;
    },

    getRank: async function (typeId, universityId, page = 0, pageSize = 300) {
        const pageNum = parseInt(page, 10);
        const pageSizeNum = parseInt(pageSize, 10);
        const offset = pageNum * pageSizeNum;
        const limit = pageSizeNum;


        const listOb = await models.session.findAll({
            where: {
                finalTime: {
                    [models.Sequelize.Op.ne]: null
                },
                typeId,
            }
        })
        const sessionsIdFinal = []
        listOb.forEach( e => {
            sessionsIdFinal.push(e.dataValues.id)
        })
        
        const info = await models.user.findAndCountAll({
            limit,
            offset,
            where: {
                sessionId: sessionsIdFinal,
                universityId
            },
            order: [
                ['score', 'DESC'],
                ['time'],
            ]
        });
        return info;
    },

    activeUserByIds: async function (ids) {
        const activated = await models.user.update(
            {
                isActive: true,
            },
            {
                where: {
                    id: ids,
                },
            },
        );

        return activated;
    },
    banUserByIds: async function (ids) {
        const activated = await models.user.update(
            {
                isActive: false,
            },
            {
                where: {
                    id: ids,
                },
            },
        );

        return activated;
    },

    /* ------------- NOT USE ------------------ */

    updateUserRoleById: async function (id, role) {
        const granted = await models.user.update(
            {
                roleId: rolesMap[role],
            },
            {
                where: {
                    id: id,
                },
            },
        );

        return granted;
    },
};
