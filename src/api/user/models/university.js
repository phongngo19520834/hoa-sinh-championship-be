module.exports = (sequelize, Sequelize) => {
    class Universities extends Sequelize.Model {}

    Universities.init(
        {
            name: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true,
            },
            description: {
                type: Sequelize.STRING(255),
            },
        },
        {
            sequelize,
            modelName: 'university',
            timestamps: false,
            underscored: true,
        },
    );

    /**
     * -------------- ASSOSIATION ----------------
     */

    Universities.associate = function (models) {
        Universities.hasMany(models.user);
    };

    return Universities;
};
