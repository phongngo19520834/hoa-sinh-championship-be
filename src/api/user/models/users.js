/* eslint-disable prettier/prettier */
module.exports = (sequelize, Sequelize) => {
    class User extends Sequelize.Model {}

    User.init(
        {
            code: {
                type: Sequelize.STRING(255),
                allowNull: false,
                unique: {
                    args: true,
                    msg: 'User with this userCode already exist.',
                },
            },
            sessionId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            universityId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            fullName: {
                type: Sequelize.STRING(255),
                allowNull: false,
            },
            roleId: {
                type: Sequelize.INTEGER,
                allowNull: false,
            },
            isActive: {
                type: Sequelize.BOOLEAN,
                defaultValue: true,
                allowNull: false,
            },
            historyQues: {
                type: Sequelize.STRING(1200),
                defaultValue: '',
                allowNull: false,
            },
            historyAnss: {
                type: Sequelize.STRING(1200),
                defaultValue: '',
                allowNull: false,
            },
            startTime: {
                type: Sequelize.DATE,
                defaultValue: null,
                allowNull: true,
            },
            time: {
                type: Sequelize.INTEGER,
                allowNull: true,
            },
            score: {
                type: Sequelize.INTEGER,
                allowNull: true,
            }
        },
        {
            sequelize,
            modelName: 'user',
            timestamps: true,
            underscored: true,
        },
    );

    User.associate = function (models) {
        User.belongsTo(models.role, {
            foreignKey: 'roleId',
            as: 'role',
            // onDelete: 'SET NULL',
        });
        User.belongsTo(models.session, {
            foreignKey: 'sessionId',
            as: 'session',
            // onDelete: 'SET NULL',
        });
        User.belongsTo(models.session, {
            foreignKey: 'universityId',
            as: 'university',
            // onDelete: 'SET NULL',
        });
        /**
         * -------------- SCOPES ----------------
         */

        User.addScope('role', {
            include: [
                {
                    model: models.role,
                    as: 'role',
                    required: true,
                    attributes: {
                        include: ['id', 'name'],
                    },
                },
            ],
        });
        User.addScope('session', {
            include: [
                {
                    model: models.session,
                    as: 'session',
                    required: true,
                    attributes: {
                        include: ['id', 'finalTime', 'examId'],
                    },
                },
            ],
        });
        User.addScope('university', {
            include: [
                {
                    model: models.university,
                    as: 'university',
                    required: true,
                    attributes: {
                        include: ['id', 'name'],
                    },
                },
            ],
        });
    };
    /**
     * -------------- CLASS METHOD ----------------
     */

    User.validateUserCredentials = async function (credentials, role) {
        const { code } = credentials;
        const user = await User.scope(['role', 'session']).findOne({ where: { code } });
        if (user && user.role.name === role) {
            return user;
        }

        return null;
    };

    return User;
};
