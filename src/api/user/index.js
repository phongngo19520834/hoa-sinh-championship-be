const router = require('express').Router();
const userCtl = require('./user.controller');
const universityCtrl = require('./university.controller');
const { checkPermission } = require('../auth/auth.permission');
const { validateUserCreate, validateUserUpdate } = require('./user.validate');
const auth = require('../auth/passport.strategy')();

router.get('/rank', userCtl.getRank)
router.get('/search', userCtl.searchUserByUserCode);

router.get('/university', universityCtrl.getAll)

router.use(auth.authenticate);

/**
 * -------------- FOR USERS ----------------
 */

router.route('/university')
    .post(checkPermission(['admin', 'editor']), universityCtrl.create)
router.route('/university/:id')
    .get(checkPermission(['user', 'editor', 'admin']), universityCtrl.getById)
    .patch(checkPermission(['admin', 'editor']), universityCtrl.updateById)
    .delete(checkPermission(['admin', 'editor']), universityCtrl.deleteById)

// Get user profile by credentials

router.get('/me', checkPermission(['user']), userCtl.whoAmI);

 // Update user profile at first login
router.patch(
    '/update-first-login',
    checkPermission(['user']),
    validateUserUpdate,
    userCtl.updateUserFirstLoginById,
);

/**
 * -------------- FOR ADMIN ----------------
 */

 // Get all user
router
    .route('/')
    .get(checkPermission(['admin', 'editor']), userCtl.getAllUser)
    .post(checkPermission(['admin', 'editor']), validateUserCreate, userCtl.create)

router.post('/create-list', checkPermission(['admin', 'editor']), userCtl.createList)

// Get user by ID
router.get('/:id', checkPermission(['admin', 'editor']), userCtl.getUserById);


// Update user by ID
router.patch('/:id', checkPermission(['admin', 'editor']), userCtl.updateUserById);

// Delete user by ID
router.delete('/:id', checkPermission(['admin', 'editor']), userCtl.deleteUserById);
// Reset result user by code
router.post('/reset-result-quiz-by-code', checkPermission(['admin']), userCtl.resetUserResultByCode);

module.exports = router;
