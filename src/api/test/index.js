const router = require('express').Router();
const auth = require('../auth/passport.strategy')();
const { checkPermission } = require('../auth/auth.permission');
const testCtl = require('./test.controller');
const {validateTestFinal} = require('./test.validate');

router.use(auth.authenticate);

router
    .route('/')
    .get(
        checkPermission(['user']),
        testCtl.getExam,
    ) // Return a random exam detail
    .post(checkPermission(['user']), validateTestFinal, testCtl.finalExam);
router.get('/check', checkPermission(['user']), testCtl.check)

module.exports = router;
