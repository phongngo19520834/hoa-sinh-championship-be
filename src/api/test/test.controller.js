const questionService = require('../question/question.service');
const sessionService = require('../session/session.service');
const userService = require('../user/user.service');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');

module.exports = {
    check: async function (req, res, next) {
        try {
            const { id } = req.user;
            const user = await userService.getUserById(id);

            const { historyQues, historyAnss } = user.dataValues
            if ( historyAnss ) { return res.json({message: 'User had submited this exam' } )}

            // check session of code user is start.
            const session = await sessionService.isCanExam(user.sessionId);
            if (!session){ return res.json({message: 'This exam is current not available' }) }

            if ( !historyQues ) {
                return res.json({message: 'you already to start exam'})
            }

            const timeServer = Date.now();
            const timeServerStart = user.dataValues.startTime
            const time = timeServer - timeServerStart;
            if (time > (session.type.dataValues.time)*60*1000){
                return res.json({message: "It's over time"});
            }
            return res.json({message: 'You are doing exam'})
        } catch (err) {
            return next(err);
        }
    },
    getExam: async function (req, res, next) {
        try {
            const { id } = req.user;
            const user = await userService.getUserById(id);

            const { historyQues, historyAnss } = user.dataValues
            if (historyAnss) {
                throw new AppError(
                    httpStatus.FORBIDDEN,
                    'User had submited this exam',
                    true,
                );
            }

            // check session of code user is start.
            const session = await sessionService.isCanExam(user.sessionId);
            if (!session){
                throw new AppError(
                    httpStatus.FORBIDDEN,
                    'This exam is current not availeble',
                    true,
                );
            }

            let questions = [];
            let timeServerStart = 0;

            if (!historyQues) { // start new exaM
                const { examId, exam } = session.dataValues;
                const { totalQuestionUserMustDo } = exam.dataValues;
                questions = await questionService.getRandomQuestionByExamId(examId, totalQuestionUserMustDo);
                const lengthQues = questions.length;
                let quesStr = '';
                for (let i=0; i<lengthQues; i+=1) {
                    quesStr += questions[i].id;
                    quesStr += (i < lengthQues - 1) ? '_' : '';
                }
                const now = new Date();
                timeServerStart = now.getTime();
                await userService.startExam(id, timeServerStart, quesStr);
                console.log(`[%]----- User with id "${id}" got exam at ${new Date()}`);
            } else { // get exam for contineu do
                const timeServer = Date.now();
                timeServerStart = user.dataValues.startTime
                const time = timeServer - timeServerStart;
                if (time > (session.type.dataValues.time)*60*1000){
                    return res.json({message: "It's over time"});
                }

                const arrayHisQues = historyQues.split('_');
                for (let i = 0; i < arrayHisQues.length; i += 1) {
                    const ele = arrayHisQues[i];
                    // eslint-disable-next-line no-await-in-loop
                    const found = await questionService.getById(ele)
                    // eslint-disable-next-line no-continue
                    if (!found) continue;

                    // add to 
                    questions.push(found)
                }
            }
            
            return res.json({
                questions,
                timeServerStart,
                time: session.type.dataValues.time
            })
        } catch (err) {
            return next(err);
        }
    },
    finalExam: async function (req, res, next) {
        try {
            const { id } = req.user;
            /* Check if user not submit any times before */
            const user = await userService.isUserCanSubmitExam(id);
            if (!user) {
                throw new AppError(
                    httpStatus.FORBIDDEN,
                    'User has completed this exam.',
                    true,
                );
            }
            const timeServerEnd = Date.now();
            const timeServerStart = new Date(user.startTime);
            let time = timeServerEnd - timeServerStart;
            const session = await sessionService.getById(user.sessionId)
            if (time > (session.type.dataValues.time)*60*1000) {
                if (time - (session.type.dataValues.time)*60*1000 > 3*60*1000){
                    return res.json({message: "It's over time"});
                }
                time = (session.type.dataValues.time)*60*1000
            }
            const { arrayAns } = req.body;
            
            const { historyQues } = user.dataValues
            const { maxScore, totalQuestionUserMustDo } = session.exam.dataValues
            const examResult = await questionService.checkQuestionToScore(
                JSON.parse(arrayAns), historyQues, maxScore, totalQuestionUserMustDo
            );
            console.log(`[%]----- User with id "${id}" submited exam at ${new Date()}`);
            // update score and time for user
            await userService.finalExam(id, time, examResult.anss, examResult.score);
            return res.json({message: 'Your answer is submited successful'});
        } catch (err) {
            return next(err);
        }
    },
};
