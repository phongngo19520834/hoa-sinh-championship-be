/* eslint-disable no-param-reassign */
const Joi = require('@hapi/joi');
const { schemaValidator } = require('../../common/schema-validator/utils');

const testFinalSchema = Joi.object({
    arrayAns: Joi.string().required(),
}).unknown(true);
module.exports = {
    validateTestFinal: function (req, res, next) {
        try {
            schemaValidator(testFinalSchema, req.body);

            next();
        } catch (err) {
            next(err);
        }
    },
};
