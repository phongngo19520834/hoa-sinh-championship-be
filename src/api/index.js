const router = require('express').Router();
const adminRouter = require('./admin');
const userRouter = require('./user');
const examRouter = require('./exam');
const questionRouter = require('./question');
const sessionRouter = require('./session');
const testRouter = require('./test');
const authRouter = require('./auth');

router.use('/auth', authRouter); // Authentication

router.use('/user', userRouter); // Users
router.use('/exam', examRouter); // Exams
router.use('/question', questionRouter); // Questions
router.use('/session', sessionRouter); // Sessions

router.use('/test', testRouter); // Test

router.use('/admin', adminRouter); // Admins

module.exports = router;
