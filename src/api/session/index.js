const router = require('express').Router();
const auth = require('../auth/passport.strategy')();
const { checkPermission } = require('../auth/auth.permission');
const sessionCtl = require('./session.controller');
const typeCtrl = require('./type.controller');
const {
    validateSessionCreate,
    validateSessionUpdate,
} = require('./session.validate');

router.use(auth.authenticate);


router.route('/type')
    .get(checkPermission(['admin', 'editor']), typeCtrl.getAll)
    .post(checkPermission(['admin', 'editor']), typeCtrl.create)
router.route('/type/:id')
    .get(checkPermission(['admin', 'editor']), typeCtrl.getById)
    .patch(checkPermission(['admin', 'editor']), typeCtrl.updateById)
    .delete(checkPermission(['admin', 'editor']), typeCtrl.deleteById)

router
    .route('/')
    .get(checkPermission(['admin', 'editor']), sessionCtl.getAll)
    .post(
        checkPermission(['admin', 'editor']),
        validateSessionCreate,
        sessionCtl.create,
    );
router
    .route('/search')
    .get(checkPermission(['admin', 'editor']), sessionCtl.searchByName);
    
router
    .route('/:id/active')
    .get(checkPermission(['admin', 'editor']), sessionCtl.activateById);
    router
    .route('/:id/final')
    .get(checkPermission(['admin', 'editor']), sessionCtl.finalSessionById);

router
    .route('/:id')
    .get(checkPermission(['admin', 'editor']), sessionCtl.getById)
    .patch(
        checkPermission(['admin', 'editor']),
        validateSessionUpdate,
        sessionCtl.updateById,
    )
    .delete(checkPermission(['admin', 'editor']), sessionCtl.deleteById);



module.exports = router;
