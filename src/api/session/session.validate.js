/* eslint-disable no-param-reassign */
const Joi = require('@hapi/joi');
const { schemaValidator } = require('../../common/schema-validator/utils');

// We only require some field when create new exam...
const sessionCreateSchema = Joi.object({
    name: Joi.string().min(1).required(),
    typeId: Joi.number().required(),
    examId: Joi.number().required(),
}).unknown(true);

//  ...but not when we update its data.
const sessionUpdateSchema = Joi.object({
    name: Joi.string().min(1),
    examId: Joi.number(),
    typeId: Joi.number()
}).unknown(true);

module.exports = {
    // Use for create
    validateSessionCreate: function (req, res, next) {
        try {
            schemaValidator(sessionCreateSchema, req.body);

            delete req.body.isActive
            delete req.body.finalTime

            return next();
        } catch (err) {
            return next(err);
        }
    },

    // Use for update
    validateSessionUpdate: function (req, res, next) {
        try {
            schemaValidator(sessionUpdateSchema, req.body);

            delete req.body.isActive
            delete req.body.finalTime

            return next();
        } catch (err) {
            return next(err);
        }
    },
};
