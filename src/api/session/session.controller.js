const sessionService = require('./session.service');
const examService = require('../exam/exam.service');

const AppErr = require('../../common/error/error');

module.exports = {
    getAll: async function (req, res, next) {
        try {
            const { page, pageSize } = req.query;
            const sessions = await sessionService.getAll(page, pageSize);

            return res.json(sessions);
        } catch (err) {
            return next(err);
        }
    },
    searchByName: async function (req, res, next) {
        try {
            const { name } = req.query;
            const sessions = await sessionService.searchByName(name);
            return res.json(sessions);
        } catch (err) {
            return next(err);
        }   
    },

    create: async function (req, res, next) {
        try {
            const session = await sessionService.create(req.body);

            return res.json(session);
        } catch (err) {
            return next(err);
        }
    },

    getById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const session = await sessionService.getById(id);

            return res.json(session);
        } catch (err) {
            return next(err);
        }
    },

    updateById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const cat = await sessionService.updateById(id, req.body);

            return res.json(cat);
        } catch (err) {
            return next(err);
        }
    },

    activateById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const session = await sessionService.getById(id);
            if (!session.examId) {
                throw new AppErr(401, 'Session not have an Exam', true)
            }
            const exam = await examService.getById(session.examId);
            if (!exam.isComplete) {
                throw new AppErr(401, 'Exam is not completed', true)
            }
            await sessionService.startSessionById(id);
            return res.json(true);
        } catch (err) {
            return next(err);
        }
    },

    finalSessionById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const now = new Date();
            const timeServer = now.getTime();
            await sessionService.finalSessionById(id, timeServer);

            return res.json(true);
        } catch (err) {
            return next(err);
        }
    },

    deleteById: async function (req, res, next) {
        try {
            const { id } = req.params;
            const affected = await sessionService.deleteById(id);

            return res.json({ affected });
        } catch (err) {
            return next(err);
        }
    },
};
