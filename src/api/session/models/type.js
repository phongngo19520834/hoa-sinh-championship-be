module.exports = (sequelize, Sequelize) => {
    class Types extends Sequelize.Model {}

    Types.init(
        {
            name: {
                type: Sequelize.STRING(50),
                allowNull: false,
                unique: true,
            },
            time: {
                type: Sequelize.INTEGER,
                allowNull: false,
                defaultValue: 10
            },
            description: {
                type: Sequelize.STRING(255),
            },
        },
        {
            sequelize,
            modelName: 'type',
            timestamps: false,
            underscored: true,
        },
    );

    /**
     * -------------- ASSOSIATION ----------------
     */

    Types.associate = function (models) {
        Types.hasMany(models.user);
    };

    return Types;
};
