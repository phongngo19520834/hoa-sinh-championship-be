const models = require('../../models');
const AppError = require('../../common/error/error');
const { httpStatus } = require('../../common/error/http-status');
const wrapCatchAsync = require('../../common/error/catchAsync'); 

module.exports = {
  
    getAll: wrapCatchAsync(async function (req, res, next) {
      const types = await models.type.findAndCountAll();
      return res.json(types);
    }),

    create: wrapCatchAsync(async function (req, res, next) {
        const type = await models.type.create(req.body);

        return res.json(type);
    }),

    getById:  wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const found = await models.type.findByPk(id);

      if (!found) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This type does not exist.',
              true,
          );
      }
      return res.json(found);
    }),

    updateById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;
      const type = await models.type.findByPk(id);
      type.set(req.body);
      const updated = await type.save();

      return res.json(updated);
    }),

    deleteById: wrapCatchAsync(async function (req, res, next) {
      const { id } = req.params;  
      const affected = await models.type.destroy({
            where: {
                id: id,
            },
        });

      if (affected === 0) {
          throw new AppError(
              httpStatus.NOT_FOUND,
              'This type does not exist.',
              true,
          );
      }

      return res.json(affected);
    }),
};
