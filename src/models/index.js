const Sequelize = require('sequelize');
const path = require('path');
const config = require('config').get('db');
const glob = require('glob');

require('dotenv').config();

const db = {};

// Create connection
const sequelize = new Sequelize(
    process.env.DB_NAME || config.database,
    process.env.DB_USERNAME || config.username,
    process.env.DB_PASSWORD || config.password,
    {
        timezone: '+07:00',
        host: process.env.DB_HOST || config.host,
        dialect: 'mysql',
        logging: false,
    },
);

// Checking if connection is established
(async function connect() {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
        console.log(process.env.DB_NAME);
    }
})();

// Import all models from each api to database
const importDbPromise = glob.sync('api/**/models/*.js', {
    cwd: process.env.NODE_PATH || 'src',
}).map( async function(file) {
    const model = require(path.join(file))(sequelize, Sequelize);
    console.log(model);
    db[model.name] = model;
});

Promise.all(importDbPromise)

Object.keys(db).forEach((modelName) => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
