const path = require('path');
const glob = require('glob');
const { rolesMap } = require('../api/admin/role.enum');

const db = require('./index');

(async function seeders() {
  try {

      const importConfigSample = glob.sync('api/**/models/*.enum.json', {
          cwd: process.env.NODE_PATH || 'src',
      })
      importConfigSample.map(async function (file) {
          const data = require(path.join(file))
          const seedData = data.data;
          const seedName = data.name
          await db[seedName].bulkCreate(seedData)
          console.log(seedData)
      })

      await Promise.all(importConfigSample)

      console.log('Seed data config')

      await db.admin.create({
          username: process.env.ADMIN_USERNAME || 'admin',
          password: process.env.ADMIN_PASSWORD || '@V3ryStR0N9P@asSWorD',
          roleId: rolesMap.admin, // user role
          isActive: true,
      });

      console.log('Admin account was seeded.');
  } catch (err) {
      if (err.name === 'SequelizeUniqueConstraintError') {
          console.log('Admin accound already exist in database.');
      } else {
          console.log(err.message);
      }
  }
})();